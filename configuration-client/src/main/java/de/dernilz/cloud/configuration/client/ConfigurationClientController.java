package de.dernilz.cloud.configuration.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigurationClientController {

    @Value("${spring.profiles.active}")
    private String activeProfiles;

    @GetMapping(value = "/hello/{name}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String hello(@PathVariable("name") String name) {
        return String.format("Hello %s!\nactiveProfiles=%s\n", name, this.activeProfiles);
    }
}
